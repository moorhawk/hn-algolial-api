# HN Algolial api

This is an un-official implementation of the
[HN Algolial API](https://hn.algolia.com/api) in Rust. This library only
implements the url generation and the api response parsing. For fetching the data
another library needs to be used like reqwest for example.

