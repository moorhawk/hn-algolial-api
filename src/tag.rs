use std::fmt::Write;

use crate::error::Error;

/*
 * Algolial supports tag filtering. You can limit the scope of the results by
 * setting the type of the items returned. The tags are delimited by commands.
 * Each comma is a logical and unless it is inside paranthesis:
 *
 * story,author_user => Story and Author = user
 * (story,author_user) => Story or Author = user
 */

pub enum Tag<'a> {
    Comment,
    Story,
    Poll,
    Pollopt,
    ShowHn,
    AskHn,
    FrontPage,
    Author(&'a str),
    NamedStory(&'a str),
}

impl Tag<'_> {
    pub fn write<W>(&self, out: &mut W) -> Result<(), Error>
    where
        W: Write,
    {
        match self {
            Self::Comment => {
                out.write_str("comment")?;
            }
            Self::Story => {
                out.write_str("story")?;
            }
            Self::Poll => {
                out.write_str("poll")?;
            }
            Self::Pollopt => {
                out.write_str("pollopt")?;
            }
            Self::ShowHn => {
                out.write_str("show_hn")?;
            }
            Self::AskHn => {
                out.write_str("ask_hn")?;
            }
            Self::FrontPage => {
                out.write_str("front_page")?;
            }
            Self::Author(author) => {
                out.write_str("author_")?;
                out.write_str(author)?;
            }
            Self::NamedStory(story_name) => {
                out.write_str("story_")?;
                out.write_str(story_name)?;
            }
        }
        Ok(())
    }
}

pub enum SubExpression<'a> {
    Tag(Tag<'a>),
    Or(Vec<Tag<'a>>),
}

pub struct TagExpression<'a> {
    expr: Vec<SubExpression<'a>>,
}

impl<'a> TagExpression<'a> {
    pub fn new() -> Self {
        Self { expr: Vec::new() }
    }

    pub fn story(&mut self) {
        self.expr.push(SubExpression::Tag(Tag::Story));
    }

    pub fn comment(&mut self) {
        self.expr.push(SubExpression::Tag(Tag::Comment));
    }

    pub fn story_id(&mut self, id: &'a str) {
        self.expr.push(SubExpression::Tag(Tag::NamedStory(id)));
    }

    pub fn author(&mut self, username: &'a str) {
        self.expr.push(SubExpression::Tag(Tag::Author(username)));
    }

    pub fn or(&mut self, tags: Vec<Tag<'a>>) {
        self.expr.push(SubExpression::Or(tags));
    }

    pub fn write<W>(&self, writer: &mut W) -> Result<(), Error>
    where
        W: Write,
    {
        for (i, s_expr) in self.expr.iter().enumerate() {
            if i != 0 {
                writer.write_char(',')?;
            }
            match s_expr {
                SubExpression::Tag(t) => t.write(writer)?,
                SubExpression::Or(v_t) => {
                    writer.write_char('(')?;
                    for (i, t) in v_t.iter().enumerate() {
                        if i != 0 {
                            writer.write_char(',')?;
                        }
                        t.write(writer)?
                    }
                    writer.write_char(')')?;
                }
            }
        }

        Ok(())
    }
}

pub struct TagExpressionBuilder<'a> {
    expr: TagExpression<'a>,
}

impl<'a> TagExpressionBuilder<'a> {
    pub fn new() -> Self {
        Self {
            expr: TagExpression::new(),
        }
    }

    pub fn story(mut self) -> Self {
        self.expr.story();
        self
    }

    pub fn comment(mut self) -> Self {
        self.expr.comment();
        self
    }

    pub fn story_id(mut self, id: &'a str) -> Self {
        self.expr.story_id(id);
        self
    }

    pub fn author(mut self, username: &'a str) -> Self {
        self.expr.author(username);
        self
    }

    pub fn or(mut self, tags: Vec<Tag<'a>>) -> Self {
        self.expr.or(tags);
        self
    }

    pub fn build(self) -> TagExpression<'a> {
        self.expr
    }
}

pub struct OrTagBuilder<'a> {
    tags: Vec<Tag<'a>>,
}

impl<'a> OrTagBuilder<'a> {
    pub fn new() -> Self {
        Self { tags: Vec::new() }
    }

    pub fn story(mut self) -> Self {
        self.tags.push(Tag::Story);
        self
    }

    pub fn comment(mut self) -> Self {
        self.tags.push(Tag::Comment);
        self
    }

    pub fn story_id(mut self, id: &'a str) -> Self {
        self.tags.push(Tag::NamedStory(id));
        self
    }

    pub fn author(mut self, username: &'a str) -> Self {
        self.tags.push(Tag::Author(username));
        self
    }

    pub fn build(self) -> Vec<Tag<'a>> {
        self.tags
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_algolial_expression() -> Result<(), Error> {
        let mut exp = TagExpression::new();
        let mut res = String::new();

        exp.story();
        exp.comment();
        exp.story_id("123456789");
        exp.author("userX");
        exp.write(&mut res)?;

        assert_eq!(res, "story,comment,story_123456789,author_userX");

        res.clear();
        let exp = TagExpressionBuilder::new()
            .comment()
            .or(OrTagBuilder::new().author("userA").author("userB").build())
            .build();
        exp.write(&mut res)?;
        assert_eq!(res, "comment,(author_userA,author_userB)");

        Ok(())
    }
}
