//! This an unofficial implementation of the Algolial Hacker news Api. It is
//! meant to help easily write queries. The crate helps you generate an url based
//! on your input. This url can then be used with the https client of your choice.
//! In the examples reqwest gets used but that library is not a requirement and you
//! may use any other library of choice.
//!
//! # Examples
//!
//! ## Fetch a user by username
//!
//! ```
//! use reqwest::blocking::Client;
//! use std::str::FromStr;
//! use hn_algolial_api::{user::User, api::HNAlgolialApi, error::Error};
//!
//! fn main() -> Result<(), Error> {
//!
//!     let client = Client::new();
//!     let mut api = HNAlgolialApi::new();
//!
//!     api.user("jl");
//!     let resp = client.get(api.url()?).send().unwrap().text().unwrap();
//!     let user = User::from_str(&resp)?;
//!     assert_eq!(user.username, "jl");
//!     assert_eq!(user.about, "This is a test");
//!     Ok(())
//! }
//! ```
//!
//! ## Fetch an Item by id
//!
//! ```
//! use reqwest::blocking::Client;
//! use std::str::FromStr;
//! use hn_algolial_api::{item::{Item, ItemType}, api::HNAlgolialApi, error::Error};
//!
//! fn main() -> Result<(), Error> {
//!
//!     let client = Client::new();
//!     let mut api = HNAlgolialApi::new();
//!
//!     api.item("2921983");
//!     let resp = client.get(api.url()?).send().unwrap().text().unwrap();
//!     let item = Item::from_str(&resp)?;
//!     assert_eq!(item.text, Some(String::from("Aw shucks, guys ... you make me blush with your compliments.<p>Tell you what, Ill make a deal: I'll keep writing if you keep reading. K?")));
//!     assert_eq!(item.title, None);
//!     assert_eq!(item.parent_id, Some(2921506));
//!     assert_eq!(item.created_at_i, 1314211127);
//!     assert_eq!(item.itype, ItemType::Comment);
//!     Ok(())
//! }
//! ```

pub mod api;
pub mod error;
pub mod hit;
pub mod item;
pub mod tag;
pub mod user;

#[cfg(test)]
mod tests {
    use crate::{
        api::HNAlgolialApi,
        error::Error,
        hit::SearchResult,
        item::{Item, ItemType},
    };
    use reqwest::blocking::Client;
    use std::str::FromStr;

    #[test]
    fn test_algolial_api() -> Result<(), Error> {
        let client = Client::new();

        let mut api = HNAlgolialApi::new();

        api.item("2921983");
        let resp = client.get(api.url()?).send().unwrap().text().unwrap();
        let item = Item::from_str(&resp)?;

        let parent_id = item.parent_id.unwrap().to_string();
        api.item(&parent_id);
        let resp = client.get(api.url()?).send().unwrap().text().unwrap();
        let parent_item = Item::from_str(&resp)?;

        assert_eq!(parent_item.itype, ItemType::Story);
        assert_eq!(
            parent_item.title,
            Some(
                "Peter Norvig on a 45-year-old article about a checkers-playing program"
                    .to_string()
            )
        );

        api.search("test");
        api.hits_per_page(1);
        let resp = client.get(api.url()?).send().unwrap().text().unwrap();

        println!("{}", resp);
        let search_res = SearchResult::from_str(&resp)?;

        assert_eq!(search_res.hits.len(), 1);
        assert_eq!(search_res.page, 0);
        assert_eq!(search_res.hits_per_page, 1);

        api.item("34529137");
        let resp = client.get(api.url()?).send().unwrap().text().unwrap();
        let _item = Item::from_str(&resp)?;

        api.item("35439958");
        let resp = client.get(api.url()?).send().unwrap().text().unwrap();
        let _item = Item::from_str(&resp)?;

        Ok(())
    }
}
