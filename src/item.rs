use serde_derive::Deserialize;
use std::str::FromStr;

use crate::error::Error;

#[derive(Deserialize, Debug, Clone, PartialEq, Eq)]
#[serde(rename_all = "camelCase")]
pub enum ItemType {
    Job,
    Story,
    Comment,
    Poll,
    Pollopt,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Item {
    pub id: u64,
    pub created_at: String,
    pub created_at_i: u64,
    #[serde(alias = "type")]
    pub itype: ItemType,
    pub author: String,
    pub title: Option<String>,
    pub url: Option<String>,
    pub text: Option<String>,
    pub points: Option<i64>,
    pub parent_id: Option<u64>,
    pub story_id: Option<u64>,
    pub children: Vec<Item>,
}

impl FromStr for Item {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(serde_json::from_str(s)?)
    }
}
