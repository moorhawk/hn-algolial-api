use std::fmt::Write;

use url::Url;

use crate::{error::Error, tag::TagExpression};

const ALGOLIAL_API_URL: &str = "https://hn.algolia.com/api/v1/";

pub enum RequestType {
    User,
    Item,
    Search,
    SortedSearch,
}

impl RequestType {
    pub fn as_str(&self) -> &str {
        match self {
            RequestType::User => "users",
            RequestType::Item => "items",
            RequestType::Search => "search",
            RequestType::SortedSearch => "search_by_date",
        }
    }
}

pub struct HNAlgolialApi<'a> {
    url: String,
    identifier: String,
    query: String,
    field_restriction: String,
    numeric_filters: String,
    req_type: RequestType,

    hits_per_page: u32,
    page: u32,
    tags: Option<TagExpression<'a>>,
}

impl<'a> HNAlgolialApi<'a> {
    pub fn new() -> Self {
        Self {
            url: String::new(),
            identifier: String::new(),
            query: String::new(),
            field_restriction: String::from("title,author,url"),
            numeric_filters: String::new(),
            req_type: RequestType::User,
            hits_per_page: 50,
            page: 0,
            tags: None,
        }
    }

    pub fn user(&mut self, identifier: &str) {
        self.req_type = RequestType::User;
        self.identifier.clear();
        self.identifier.push_str(identifier);
    }

    pub fn item(&mut self, identifier: &str) {
        self.req_type = RequestType::Item;
        self.identifier.clear();
        self.identifier.push_str(identifier);
    }

    pub fn search(&mut self, query: &str) {
        self.req_type = RequestType::Search;
        self.identifier.clear();
        self.query.clear();
        self.query.push_str(query);
    }

    pub fn sorted_search(&mut self, query: &str) {
        self.req_type = RequestType::SortedSearch;
        self.identifier.clear();
        self.query.clear();
        self.query.push_str(query);
    }

    pub fn hits_per_page(&mut self, hits: u32) {
        self.hits_per_page = hits;
    }

    pub fn field_restrictions(&mut self, restriction: &str) {
        self.field_restriction.clear();
        self.field_restriction.push_str(restriction);
    }

    pub fn numeric_filters(&mut self, f: &str) {
        self.numeric_filters.clear();
        self.numeric_filters.push_str(f);
    }

    pub fn before_timestamp(&mut self, timestamp: &str) {
        if ! self.numeric_filters.is_empty() {
            self.numeric_filters.push_str(",");
        }
        self.numeric_filters.push_str("created_at_i<=");
        self.numeric_filters.push_str(timestamp);
    }

    pub fn after_timestamp(&mut self, timestamp: &str) {
        if ! self.numeric_filters.is_empty() {
            self.numeric_filters.push_str(",");
        }
        self.numeric_filters.push_str("created_at_i>=");
        self.numeric_filters.push_str(timestamp);
    }

    pub fn page(&mut self, page: u32) {
        self.page = page;
    }

    pub fn tag(&mut self, tags: TagExpression<'a>) {
        self.tags = Some(tags);
    }

    pub fn url(&mut self) -> Result<Url, Error> {
        self.url.clear();
        self.url.push_str(ALGOLIAL_API_URL);

        self.url.push_str(self.req_type.as_str());

        if matches!(self.req_type, RequestType::User | RequestType::Item) {
            self.url.push('/');
            self.url.push_str(&self.identifier);
            return Ok(Url::parse(&self.url)?);
        }

        /* Search or Sorted Search have same query format */

        self.url.push_str("?hitsPerPage=");
        write!(self.url, "{}", self.hits_per_page)?;

        if !self.query.is_empty() {
            self.url.push_str("&query=");
            self.url.push_str(&self.query);
        }

        if !self.field_restriction.is_empty() {
            self.url.push_str("&restrictSearchableAttributes=");
            self.url.push_str(&self.field_restriction);
        }

        if let Some(tags) = &self.tags {
            self.url.push_str("&tags=");
            tags.write(&mut self.url)?;
        }

        if self.page != 0 {
            self.url.push_str("&page=");
            write!(self.url, "{}", self.page)?;
        }

        if !self.numeric_filters.is_empty() {
            self.url.push_str("&numericFilters=");
            self.url.push_str(&self.numeric_filters);
        }

        Ok(Url::parse(&self.url)?)
    }
}

impl Default for HNAlgolialApi<'_> {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tag::{OrTagBuilder, TagExpressionBuilder};

    #[test]
    fn test_algolial_api_url() -> Result<(), Error> {
        let mut api = HNAlgolialApi::new();
        api.item("tt");
        api.user("myuser");
        assert_eq!(
            api.url()?,
            Url::parse("https://hn.algolia.com/api/v1/users/myuser")?
        );

        api.user("test");
        api.item("myitem");
        assert_eq!(
            api.url()?,
            Url::parse("https://hn.algolia.com/api/v1/items/myitem")?
        );

        api.user("test");
        api.item("myitem");
        api.search("pattern");
        assert_eq!(
            api.url()?,
            Url::parse("https://hn.algolia.com/api/v1/search?hitsPerPage=50&query=pattern&restrictSearchableAttributes=title,author,url")?
        );

        api.sorted_search("pattern");
        api.hits_per_page(10);
        assert_eq!(
            api.url()?,
            Url::parse(
                "https://hn.algolia.com/api/v1/search_by_date?hitsPerPage=10&query=pattern&restrictSearchableAttributes=title,author,url"
            )?
        );

        api.page(6);
        assert_eq!(
            api.url()?,
            Url::parse(
                "https://hn.algolia.com/api/v1/search_by_date?hitsPerPage=10&query=pattern&restrictSearchableAttributes=title,author,url&page=6"
            )?
        );

        api.field_restrictions("");
        assert_eq!(
            api.url()?,
            Url::parse(
                "https://hn.algolia.com/api/v1/search_by_date?hitsPerPage=10&query=pattern&page=6"
            )?
        );

        api.tag(
            TagExpressionBuilder::new()
                .comment()
                .or(OrTagBuilder::new()
                    .author("AndyKelley")
                    .author("WalterBright")
                    .build())
                .build(),
        );
        assert_eq!(
            api.url()?,
            Url::parse(
                "https://hn.algolia.com/api/v1/search_by_date?hitsPerPage=10&query=pattern&tags=comment,(author_AndyKelley,author_WalterBright)&page=6"
            )?
        );

        api.numeric_filters("created_at_i>=123456789");
        assert_eq!(
            api.url()?,
            Url::parse(
                "https://hn.algolia.com/api/v1/search_by_date?hitsPerPage=10&query=pattern&tags=comment,(author_AndyKelley,author_WalterBright)&page=6&numericFilters=created_at_i>=123456789"
            )?
        );

        api.numeric_filters("");
        api.before_timestamp("0");
        api.after_timestamp("2");
        assert_eq!(
            api.url()?,
            Url::parse(
                "https://hn.algolia.com/api/v1/search_by_date?hitsPerPage=10&query=pattern&tags=comment,(author_AndyKelley,author_WalterBright)&page=6&numericFilters=created_at_i<=0,created_at_i>=2"
            )?
        );

        Ok(())
    }
}
