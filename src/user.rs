use serde_derive::Deserialize;
use std::str::FromStr;

use crate::error::Error;

#[derive(Deserialize, Debug)]
pub struct User {
    pub username: String,
    pub about: String,
    pub karma: u64,
}

impl FromStr for User {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(serde_json::from_str(s)?)
    }
}
