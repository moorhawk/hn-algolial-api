use thiserror;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("IO error")]
    Io(#[from] std::io::Error),

    #[error("Integer Parsing Error")]
    Integer(#[from] std::num::ParseIntError),

    #[error("JSON parsing")]
    Json(#[from] serde_json::error::Error),

    #[error("Url parse Error")]
    ParseUrl(#[from] url::ParseError),

    #[error("Format parsing Error")]
    Fmt(#[from] std::fmt::Error),
}
