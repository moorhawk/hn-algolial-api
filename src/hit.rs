use serde_derive::Deserialize;
use std::str::FromStr;

use crate::error::Error;

#[derive(Deserialize, Debug)]
pub struct Highlight {
    pub value: String,
    #[serde(alias = "matchLevel")]
    pub match_level: String,
    #[serde(alias = "fullyHighlighted")]
    pub fully_highlighted: Option<bool>,
    #[serde(alias = "matchedWords")]
    pub matched_words: Vec<String>,
}

#[derive(Deserialize, Debug)]
pub struct HighlightResult {
    pub author: Option<Highlight>,
    pub comment_text: Option<Highlight>,
    pub story_title: Option<Highlight>,
    pub story_url: Option<Highlight>,
    pub title: Option<Highlight>,
    pub url: Option<Highlight>,
}

#[derive(Deserialize, Debug)]
pub struct Hit {
    pub created_at: String,
    pub title: Option<String>,
    pub url: Option<String>,
    pub author: String,
    pub points: Option<u64>,
    pub story_text: Option<String>,
    pub comment_text: Option<String>,
    pub num_comments: Option<u64>,
    pub story_id: Option<u64>,
    pub story_title: Option<String>,
    pub story_url: Option<String>,
    pub parent_id: Option<u64>,
    pub created_at_i: u64,
    pub relevancy_score: Option<u64>,
    pub tags: Option<Vec<String>>,
    #[serde(alias = "objectID")]
    pub object_id: String,
    #[serde(alias = "_highlightResult")]
    _highlight_result: Option<HighlightResult>,
}

#[derive(Deserialize, Debug)]
pub struct SearchResult {
    pub hits: Vec<Hit>,

    #[serde(alias = "nbHits")]
    pub nb_hits: u64,
    pub page: u64,
    #[serde(alias = "nbPages")]
    pub nb_pages: u64,
    #[serde(alias = "hitsPerPage")]
    pub hits_per_page: u64,
    #[serde(alias = "exhaustiveNbHits")]
    pub exhaustive_nb_hits: bool,
    #[serde(alias = "exhaustiveTypo")]
    pub exhaustive_typo: bool,

    pub query: String,
    pub params: String,
    #[serde(alias = "processingTimeMS")]
    pub processing_time_ms: u64,
    #[serde(skip, alias = "processingTimingsMS")]
    pub processing_timings_ms: Option<u64>,
}

impl FromStr for SearchResult {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(serde_json::from_str(s)?)
    }
}
